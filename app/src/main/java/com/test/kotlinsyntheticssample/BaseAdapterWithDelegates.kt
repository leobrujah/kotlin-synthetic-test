package com.test.kotlinsyntheticssample

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.collection.SparseArrayCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

abstract class BaseAdapterWithDelegates : RecyclerView.Adapter<BaseAdapterWithDelegates.ExtensionViewHolder>() {

    protected val items: MutableList<BaseViewType> = mutableListOf()

    protected val delegateAdapters: SparseArrayCompat<BaseDelegateAdapter> = SparseArrayCompat()

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: BaseAdapterWithDelegates.ExtensionViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseAdapterWithDelegates.ExtensionViewHolder =
        delegateAdapters[viewType]?.getLayoutRes()?.let { ExtensionViewHolder(parent, it) }
            ?: throw RuntimeException("No adapter mapped to viewType: $viewType")

    override fun getItemViewType(position: Int): Int = items[position].getViewType()

    fun setItems(items: List<BaseViewType>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    inner class ExtensionViewHolder(
        parent: ViewGroup,
        @LayoutRes layoutRes: Int
    ) : RecyclerView.ViewHolder(parent.inflate(layoutRes)), LayoutContainer {
        override val containerView: View
            get() = itemView

        fun bind(item: BaseViewType) = delegateAdapters[item.getViewType()]?.bindView(containerView, item, this)
    }

}

interface BaseDelegateAdapter {
    @LayoutRes
    fun getLayoutRes(): Int

    fun bindView(itemView: View, item: BaseViewType, viewHolder: RecyclerView.ViewHolder)
}

interface BaseViewType {
    fun getViewType(): Int
}


@JvmOverloads
fun ViewGroup.inflate(@LayoutRes layoutResource: Int, attachToRoot: Boolean = false): View =
    LayoutInflater.from(context).inflate(layoutResource, this, attachToRoot)