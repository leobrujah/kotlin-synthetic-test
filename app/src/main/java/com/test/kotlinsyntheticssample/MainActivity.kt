package com.test.kotlinsyntheticssample

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_content.*
import kotlinx.android.synthetic.main.item_content.view.*

class MainActivity : AppCompatActivity() {
    val adapter by lazy { CustomDummyAdapter(DummyDelegatedAdapter()) }
    private val groupieAdapter by lazy { GroupAdapter<ViewHolder>() }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val dummymodelList = arrayListOf<DummyModel>()
        val dummymodelGroupieList = arrayListOf<GroupieDummyModel>()
        repeat(100000) {
            dummymodelList.add(DummyModel())
            dummymodelGroupieList.add(GroupieDummyModel())
        }
        adapter.setItems(dummymodelList)
        groupieAdapter.addAll(dummymodelGroupieList)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }
}

class CustomDummyAdapter constructor(
    private val dummyDelegatedAdapter: DummyDelegatedAdapter
) : BaseAdapterWithDelegates() {

    init {
        delegateAdapters.apply {
            put(DummyModel.VIEW_TYPE, dummyDelegatedAdapter)

        }
    }
}

class DummyDelegatedAdapter constructor() : BaseDelegateAdapter {

    override fun getLayoutRes(): Int = R.layout.item_content

    override fun bindView(itemView: View, item: BaseViewType, viewHolder: RecyclerView.ViewHolder) {
        with(itemView) {
            if (item is DummyModel) {
                //Here it doesn't use cached even with ViewHolder implementing LayoutContainer
                textView.text = item.content
            }
        }
    }
}

data class DummyModel(val content: String = "Hello World") : BaseViewType {
    companion object {
        @JvmStatic
        val VIEW_TYPE = DummyModel::class.hashCode()
    }

    override fun getViewType() = VIEW_TYPE
}

class GroupieDummyModel(val content: String = "Hello World") : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.textView.text = content
    }

    override fun getLayout(): Int = R.layout.item_content
}

class CachedViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bind(model: DummyModel) {
        textView.text = model.content
    }
}
